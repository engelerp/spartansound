----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:50:59 05/28/2018 
-- Design Name: 
-- Module Name:    LoadCounter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LoadCounter is
    Port ( CLK : in  STD_LOGIC; 										--Clock that triggers loading. Connect to DATA_READY_PULSE output of the serial receiver
			  CHANNEL: in STD_LOGIC;									--Channel we are loading to. If it changes, the loading counter is reset. Supply from PlaybackCounter
           ADDR_OUT : out  STD_LOGIC_VECTOR (12 downto 0));	--Loading address output. Increments with each rising edge on CLK
end LoadCounter;

architecture Behavioral of LoadCounter is
signal addr : STD_LOGIC_VECTOR (12 downto 0);					--internal signal for the loading address
signal past_chan : STD_LOGIC := '0';								--store last value of CHANNEL to detect changes without having to listen for edges

begin

transition: process(CLK)
begin
	if rising_edge(CLK) then			--updates are synchronized with the input clock
		if past_chan /= CHANNEL then	--if the channel has changed, we reset
			addr <= (others => '0');
		else
			addr <= addr + 1;				--increment the address there is no need for reset
		end if;
		past_chan <= CHANNEL;			--update the past_chan to the current CHANNEL
	end if;
end process;

ADDR_OUT <= addr;							--set the output

end Behavioral;

