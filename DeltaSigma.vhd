----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:21:58 05/28/2018 
-- Design Name: 
-- Module Name:    DeltaSigma - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DeltaSigma is
    Port ( value : in  STD_LOGIC_VECTOR (7 downto 0);						--The value we want to generate (0-255, 0-3.3V)
           clk : in  STD_LOGIC;													--Updates happen synchronous to this clock. Use fastest clock available.
           bitstream : out  STD_LOGIC);										--Output that will be at the desired voltage
end DeltaSigma;

architecture Behavioral of DeltaSigma is
signal sigma : STD_LOGIC_VECTOR (8 downto 0) := (others => '0');		--we add value to this again and again, and the overflow bit 8 is the output
signal sigmanx : STD_LOGIC_VECTOR (8 downto 0);								--the next state, sigma+value
signal expandedinput : STD_LOGIC_VECTOR (8 downto 0);						--this stores the input with 1 bit more to be compatible in math operations
begin

transition: process(clk)
begin
	if rising_edge(clk) then							--update on the clock
		sigma(7 downto 0) <= sigmanx(7 downto 0);
	end if;
end process;

expandedinput(7 downto 0) <= value;
expandedinput(8) <= '0';
sigmanx <= expandedinput + sigma;
bitstream <= sigmanx(8);								--the overflow bit is the output

end Behavioral;
