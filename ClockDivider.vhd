----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:26:01 05/28/2018 
-- Design Name: 
-- Module Name:    ClockDivider - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--clock supply to a serial receiver at 2MHz
entity ClockDivider is
    Port ( DATA_READY : in  STD_LOGIC; --1 if reception finished
           RX : in  STD_LOGIC; --reception
           CLK_IN : in  STD_LOGIC; --100MHz clock input
           CLK_SR : out  STD_LOGIC); --2MHz clock output
end ClockDivider;

architecture Behavioral of ClockDivider is
signal state : STD_LOGIC_VECTOR (5 downto 0) := (others => '0');
signal nxstate : STD_LOGIC_VECTOR (5 downto 0);
signal mask : STD_LOGIC_VECTOR (4 downto 0); --1 if we are still counting, 0 if we are done
signal maxreach : STD_LOGIC; --1 if we have counted to 25 CLK cycles (i.e. one SRCLK cycle has passed)
begin

transition: process(CLK_IN)
begin
	if rising_edge(CLK_IN) then
		state <= nxstate;
	end if;
end process;

--maxreach <= (state(8)) and (state(7))  and (not state(6)) and (state(5)) and (state(4)) and (not state(3))  and (not state(2)) and (state(1)) and (not state(0));
maxreach <= (state(4)) and (state(3)) and (not state(2)) and (not state(1)) and (state(0));
nxstate(5) <= (state(5) XOR maxreach) AND (not (RX AND DATA_READY));
mask <= (others => ((not maxreach) AND (not (RX AND DATA_READY))));
nxstate(4 downto 0) <= (state(4 downto 0) + 1) AND mask;
CLK_SR <= state(5);
end Behavioral;
