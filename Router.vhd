----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:28:39 05/28/2018 
-- Design Name: 
-- Module Name:    Router - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Router is
    Port ( CHANNEL : in  STD_LOGIC;										--select channels. for CHANNEL=1, A is playing and B is loading. Vice versa for CHANNEL=0
           ADDR_PLAY_IN : in  STD_LOGIC_VECTOR (12 downto 0);	--address counter in for playback
           ADDR_LOAD_IN : in  STD_LOGIC_VECTOR (12 downto 0);	--address counter in for loading new data
           DATA_IN : in  STD_LOGIC_VECTOR (7 downto 0);			--input of data to be loaded
			  DATA_READY_PULSE : in STD_LOGIC;							--data ready pulse of the serial receiver, used to trigger writes in the loading channel
           DATA_OUT : out  STD_LOGIC_VECTOR (7 downto 0);		--output of data to be loaded, directly connected to DATA_IN
           ADDRA_OUT : out  STD_LOGIC_VECTOR (12 downto 0);		--address output for memory A
           ADDRB_OUT : out  STD_LOGIC_VECTOR (12 downto 0);		--address output for memory B
           WRITEA_OUT : out  STD_LOGIC;								--write signal output for memory A
           WRITEB_OUT : out  STD_LOGIC);								--write signal output for memory B
end Router;

architecture Behavioral of Router is
--vector with all bits=CHANNEL
signal channel_vec: STD_LOGIC_VECTOR (12 downto 0);

begin
ADDRA_OUT <= (ADDR_PLAY_IN and channel_vec) or (ADDR_LOAD_IN and (not channel_vec));
ADDRB_OUT <= (ADDR_LOAD_IN and channel_vec) or (ADDR_PLAY_IN and (not channel_vec));

--the write signal is held low for the channel that is playing and connected to the DATA_READY_PULSE for the channel that is loading
WRITEA_OUT <= DATA_READY_PULSE and (not CHANNEL);
WRITEB_OUT <= DATA_READY_PULSE and CHANNEL;

--DATA_OUT is connected directly to DATA_IN
DATA_OUT <= DATA_IN;

--we need to cast CHANNEL to a vector to perform logic operations on vectors
channel_vec(0) <= CHANNEL;
channel_vec(1) <= CHANNEL;
channel_vec(2) <= CHANNEL;
channel_vec(3) <= CHANNEL;
channel_vec(4) <= CHANNEL;
channel_vec(5) <= CHANNEL;
channel_vec(6) <= CHANNEL;
channel_vec(7) <= CHANNEL;
channel_vec(8) <= CHANNEL;
channel_vec(9) <= CHANNEL;
channel_vec(10) <= CHANNEL;
channel_vec(11) <= CHANNEL;
channel_vec(12) <= CHANNEL;

end Behavioral;

