----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:08:36 05/28/2018 
-- Design Name: 
-- Module Name:    PlaybackClock - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--clock supply to a serial receiver at 2MHz
entity PlaybackClock is
    Port ( CLK_IN : in  STD_LOGIC; --100MHz clock input
           CLK_OUT : out  STD_LOGIC); --44100Hz clock output
end PlaybackClock;

architecture Behavioral of PlaybackClock is
signal state : STD_LOGIC_VECTOR (11 downto 0) := (others => '0');
signal nxstate : STD_LOGIC_VECTOR (11 downto 0);
signal mask : STD_LOGIC_VECTOR (10 downto 0); --1 if we are still counting, 0 if we are done
signal maxreach : STD_LOGIC; --1 if we have counted to 1134 CLK cycles (i.e. one PLAYCLK cycle has passed)
begin

--the clock triggers counting transitions
transition: process(CLK_IN)
begin
	if rising_edge(CLK_IN) then
		state <= nxstate;
	end if;
end process;

--when we have reached 10001101110=1134 (i.e. maxreach = 1), the playback clock ticks once, so we flip the output bit 11 and reset the less significant bits
maxreach <= (state(10)) AND (not state(9)) AND (not state(8)) AND (not state(7)) AND (state(6)) AND (state(5)) AND (not state(4)) AND (state(3)) AND (state(2)) AND (state(1)) AND (not state(0));
--flip output bit if the clock has ticked
nxstate(11) <= (state(11) XOR maxreach);
mask <= (others => (not maxreach));
--count +1 and reset bits 0-10 if the clock has ticked
nxstate(10 downto 0) <= (state(10 downto 0) + 1) AND mask;
--set the output
CLK_OUT <= state(11);
end Behavioral;
