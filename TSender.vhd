----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:33:04 05/28/2018 
-- Design Name: 
-- Module Name:    TSender - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TSender is
    Port ( DATA : out  STD_LOGIC_VECTOR (7 downto 0);			--The character we send. This is kept constant at 't' (01110100)
           SEND : out  STD_LOGIC;									--Send pulse. Connect to SEND input of the serial transmitter
           TRIGGER : in  STD_LOGIC;									--Triggers sending the character on both edges
			  CLK : in STD_LOGIC;										--All updates happen synchronous to this clock. Use fastest clock available
           STOP : in  STD_LOGIC);									--On high, the internal send signal is set to 0. Connect to TransmissionFinished (TF) signal of serial transmitter
end TSender;

architecture Behavioral of TSender is

signal T: STD_LOGIC_VECTOR(7 downto 0) := "01110100";	--'t'
signal old_trigger: STD_LOGIC := '1';						--old trigger to detect changes on the TRIGGER
signal send_internal: STD_LOGIC := '0';					--internal representation of SEND
begin

send_proc: process(CLK)
begin
	if rising_edge(CLK) then				--updates are done synchronous to the clock
		if trigger /= old_trigger then	--if we are triggered, send and update the old_trigger
			send_internal <= '1';
			old_trigger <= TRIGGER;
		elsif STOP = '1' then				--stop sending if stopped
			send_internal <= '0';
		end if;
	end if;
end process;
send <= send_internal;						--set the outputs
DATA <= T;

end Behavioral;

