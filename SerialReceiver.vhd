----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:22:05 05/28/2018 
-- Design Name: 
-- Module Name:    SerialReceiver - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SerialReceiver is
    Port ( RX : in  STD_LOGIC; --serial port input
           CLK : in  STD_LOGIC; --clock input
           DATA_READY : out  STD_LOGIC; --data ready output
			  DATA_READY_PULSE : out STD_LOGIC; --goes high when a full byte has been received
           DATA : out  STD_LOGIC_VECTOR (7 downto 0)); --output
end SerialReceiver;

architecture Behavioral of SerialReceiver is
signal state : STD_LOGIC_VECTOR (17 downto 0) := "000000001000000000"; 	-- read data [17:10]		DATA_READY		DATA_READY_PULSE		read mask [7:0]
																								-- read data: the data we have read and will send to the Data port
																								-- DATA_READY: is high when no reception is being processed
																								-- DATA_READY_PULSE: pulses high 1 SRCLK cycle after the data is ready on Data port
																								-- read mask: masks the current bit of the packet we are reading with a 1
																								-- note: the start bit (low) is discarded (read masked DR)
																								-- the '1' bit starts at bit 9, then goes to 0, 1, 2 ... 7, DATA_READY_PULSE, DATA_READY 
signal nxstate : STD_LOGIC_VECTOR (17 downto 0);
signal SRCLK : STD_LOGIC;
signal DRI : STD_LOGIC;

component ClockDivider is
    Port ( DATA_READY : in  STD_LOGIC;
           RX : in  STD_LOGIC;
           CLK_IN : in  STD_LOGIC;
           CLK_SR : out  STD_LOGIC);
end component;

begin
cldivider : ClockDivider port map (	DATA_READY => DRI,
												RX => Rx,
												CLK_IN => CLK,
												CLK_SR => SRCLK);
transition: process(SRCLK)
begin
	if rising_edge(SRCLK) then
		state <= nxstate;
	end if;
end process;

nxstate(9 downto 1) <= state(8 downto 0);
nxstate(0) <= state(9);

nxstate(10) <= (state(10) AND (not state(0))) OR (RX AND state(0));
nxstate(11) <= (state(11) AND (not state(1))) OR (RX AND state(1));
nxstate(12) <= (state(12) AND (not state(2))) OR (RX AND state(2));
nxstate(13) <= (state(13) AND (not state(3))) OR (RX AND state(3));
nxstate(14) <= (state(14) AND (not state(4))) OR (RX AND state(4));
nxstate(15) <= (state(15) AND (not state(5))) OR (RX AND state(5));
nxstate(16) <= (state(16) AND (not state(6))) OR (RX AND state(6));
nxstate(17) <= (state(17) AND (not state(7))) OR (RX AND state(7));
DRI <= state(9);
DATA_READY <= state(9);
DATA <= state(17 downto 10);
DATA_READY_PULSE <= state(8);
end Behavioral;
