----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:52:48 05/28/2018 
-- Design Name: 
-- Module Name:    Multiplexer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Multiplexer is
    Port ( CHANNEL : in  STD_LOGIC;									--select the channel to transmit
           DATA0_IN : in  STD_LOGIC_VECTOR (7 downto 0);		--data in for channel 0
           DATA1_IN : in  STD_LOGIC_VECTOR (7 downto 0);		--data in for channel 1
           DATA_OUT : out  STD_LOGIC_VECTOR (7 downto 0));	--data output
end Multiplexer;

architecture Behavioral of Multiplexer is
signal channel_vec: STD_LOGIC_VECTOR (7 downto 0);

begin
DATA_OUT <= (DATA1_IN and channel_vec) or (DATA0_IN and (not channel_vec));

--need to cast channel to a vector for the above logic operations
channel_vec(0) <= CHANNEL;
channel_vec(1) <= CHANNEL;
channel_vec(2) <= CHANNEL;
channel_vec(3) <= CHANNEL;
channel_vec(4) <= CHANNEL;
channel_vec(5) <= CHANNEL;
channel_vec(6) <= CHANNEL;
channel_vec(7) <= CHANNEL;

end Behavioral;

