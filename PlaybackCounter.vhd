----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:56:56 05/28/2018 
-- Design Name: 
-- Module Name:    PlaybackCounter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity PlaybackCounter is
    Port ( CLK : in  STD_LOGIC; 										--play clock input, i.e. 44100Hz
           RESET : in  STD_LOGIC;									--reset the counter on high
           ADDR_OUT : out  STD_LOGIC_VECTOR (12 downto 0);	--playback address output, incremented with 44100Hz
           CHANNEL : out  STD_LOGIC);								--channel currently selected for playback. if a full buffer has been played (8192B), it is flipped
end PlaybackCounter;

architecture Behavioral of PlaybackCounter is
signal addr : STD_LOGIC_VECTOR (13 downto 0);

begin

transition: process(CLK, RESET)
begin
	if RESET = '1' then					--reset the counter if requested
		addr <= (others => '0');
	else if rising_edge(CLK) then		--upon a clock tick, increment the playback address
			addr <= addr + 1;
		end if;
	end if;
end process;
CHANNEL <= addr(13);						--when the 13bit address counter overflows, a full buffer has been played and we swap, so CHANNEL is bit 14
ADDR_OUT <= addr(12 downto 0);		--set the address output

end Behavioral;

