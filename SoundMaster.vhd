----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:07:11 05/28/2018 
-- Design Name: 
-- Module Name:    SoundMaster - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SoundMaster is
    Port ( LED0 : out  STD_LOGIC;
           LED1 : out  STD_LOGIC;
           LED2 : out  STD_LOGIC;
           LED3 : out  STD_LOGIC;
           LED4 : out  STD_LOGIC;
           LED5 : out  STD_LOGIC;
           LED6 : out  STD_LOGIC;
           LED7 : out  STD_LOGIC;
           TXN : out  STD_LOGIC;
           RXN : in  STD_LOGIC;
			  CLK : STD_LOGIC;
           VOUTN : out  STD_LOGIC);
end SoundMaster;

architecture Behavioral of SoundMaster is

component SerialReceiver is
    Port ( RX : in  STD_LOGIC; --serial port input
           CLK : in  STD_LOGIC; --clock input
           DATA_READY : out  STD_LOGIC; --data ready output
			  DATA_READY_PULSE : out STD_LOGIC; --goes high when a full byte has been received
           DATA : out  STD_LOGIC_VECTOR (7 downto 0)); --output
end component;

component SerialTransmitter is
    Port ( DATA : in  STD_LOGIC_VECTOR (7 downto 0); --Packet to send. This has to be kept constant during the transmission by the sending device.
           SEND : in  STD_LOGIC; --Trigger sending on high
           CLK : in  STD_LOGIC; --200MHz clock
           BUSY : out  STD_LOGIC; --Sending is in progress
           TF : out  STD_LOGIC; --Pulses when transmission is finished
           TX : out  STD_LOGIC); --Serial output channel
end component;

component TSender is
    Port ( DATA : out  STD_LOGIC_VECTOR (7 downto 0);
           SEND : out  STD_LOGIC;
           TRIGGER : in  STD_LOGIC;
			  CLK : in STD_LOGIC;
           STOP : in  STD_LOGIC);
end component;

component PlaybackClock is
    Port ( CLK_IN : in  STD_LOGIC; --100MHz clock input
           CLK_OUT : out  STD_LOGIC); --44100Hz clock output
end component;

component PlaybackCounter is
    Port ( CLK : in  STD_LOGIC;
           RESET : in  STD_LOGIC;
           ADDR_OUT : out  STD_LOGIC_VECTOR (12 downto 0);
           CHANNEL : out  STD_LOGIC);
end component;

component LoadCounter is
    Port ( CLK : in  STD_LOGIC; --bring the DATA_READY here
			  CHANNEL: in STD_LOGIC;
           ADDR_OUT : out  STD_LOGIC_VECTOR (12 downto 0));
end component;

component Router is
    Port ( CHANNEL : in  STD_LOGIC;
           ADDR_PLAY_IN : in  STD_LOGIC_VECTOR (12 downto 0);
           ADDR_LOAD_IN : in  STD_LOGIC_VECTOR (12 downto 0);
           DATA_IN : in  STD_LOGIC_VECTOR (7 downto 0);
			  DATA_READY_PULSE : in STD_LOGIC;
           DATA_OUT : out  STD_LOGIC_VECTOR (7 downto 0);
           ADDRA_OUT : out  STD_LOGIC_VECTOR (12 downto 0);
           ADDRB_OUT : out  STD_LOGIC_VECTOR (12 downto 0);
           WRITEA_OUT : out  STD_LOGIC;
           WRITEB_OUT : out  STD_LOGIC);
end component;

component Memory is
	Port (	ADDRA: in STD_LOGIC_VECTOR(12 downto 0);
				DINA: in STD_LOGIC_VECTOR(7 downto 0);
				WEA: in STD_LOGIC_VECTOR(0 downto 0);
				CLKA: in STD_LOGIC;
				DOUTA: out STD_LOGIC_VECTOR(7 downto 0));
end component;

component Multiplexer is
    Port ( CHANNEL : in  STD_LOGIC;
           DATA0_IN : in  STD_LOGIC_VECTOR (7 downto 0);
           DATA1_IN : in  STD_LOGIC_VECTOR (7 downto 0);
           DATA_OUT : out  STD_LOGIC_VECTOR (7 downto 0));
end component;

component DeltaSigma is
    Port ( value : in  STD_LOGIC_VECTOR (7 downto 0);
           clk : in  STD_LOGIC;
           bitstream : out  STD_LOGIC);
end component;


signal channel_net			: STD_LOGIC;
signal data_router_out_net : STD_LOGIC_VECTOR(7 downto 0);
signal delta_sigma_in 		: STD_LOGIC_VECTOR(7 downto 0);
signal write_enable_a		: STD_LOGIC_VECTOR(0 downto 0);
signal write_enable_b		: STD_LOGIC_VECTOR(0 downto 0);
signal write_enable_bool_a : STD_LOGIC;
signal write_enable_bool_b : STD_LOGIC;
signal bufA_out				: STD_LOGIC_VECTOR(7 downto 0);
signal bufB_out				: STD_LOGIC_VECTOR(7 downto 0);
signal play_addr_net			: STD_LOGIC_VECTOR(12 downto 0);
signal load_addr_net			: STD_LOGIC_VECTOR(12 downto 0);
signal data_net				: STD_LOGIC_VECTOR(7 downto 0);
signal data_ready_pulse_net : STD_LOGIC;
signal addra_out_net			: STD_LOGIC_VECTOR(12 downto 0);
signal addrb_out_net			: STD_LOGIC_VECTOR(12 downto 0);
signal play_clock_net		: STD_LOGIC;
signal transmission_net 	: STD_LOGIC_VECTOR(7 downto 0);
signal transmission_send_net : STD_LOGIC;
signal transmission_stop_net : STD_LOGIC;


begin

write_enable_a(0) <= write_enable_bool_a;
write_enable_b(0) <= write_enable_bool_b;

Mux : Multiplexer port map (		CHANNEL => channel_net,
											DATA0_IN => bufB_out,
											DATA1_IN => bufA_out,
											DATA_OUT => delta_sigma_in);

BufA: Memory port map		(		ADDRA => addra_out_net,
											DINA => data_router_out_net,
											WEA => write_enable_a,
											CLKA => CLK,
											DOUTA => bufA_out);

BufB: Memory port map		(		ADDRA => addrb_out_net,
											DINA => data_router_out_net,
											WEA => write_enable_b,
											CLKA => CLK,
											DOUTA => bufB_out);


Route: Router port map		(		CHANNEL => channel_net,
											ADDR_PLAY_IN => play_addr_net,
											ADDR_LOAD_IN => load_addr_net,
											DATA_IN => data_net,
											DATA_READY_PULSE => data_ready_pulse_net,
											DATA_OUT => data_router_out_net,
											ADDRA_OUT => addra_out_net,
											ADDRB_OUT => addrb_out_net,
											WRITEA_OUT => write_enable_bool_a,
											WRITEB_OUT => write_enable_bool_b);

PlayClock: PlaybackClock port map(		CLK_IN => CLK,
													CLK_OUT => play_clock_net);

PlayCount: PlaybackCounter port map(	CLK => play_clock_net,
													RESET => '0',
													ADDR_OUT => play_addr_net,
													CHANNEL => channel_net);

LoadCount: LoadCounter port map(	CLK => data_ready_pulse_net,
											CHANNEL => channel_net,
											ADDR_OUT => load_addr_net);

TSend: TSender port map(	DATA => transmission_net,
									SEND => transmission_send_net,
									TRIGGER => channel_net,
									CLK => CLK,
									STOP => transmission_stop_net);

		
STransmitter: SerialTransmitter port map(	DATA => transmission_net,
														SEND => transmission_send_net,
														CLK => CLK,
														BUSY => open,
														TF => transmission_stop_net,
														TX => TXN);

SReceiver: SerialReceiver port map(	RX => RXN,
												CLK => CLK,
												DATA_READY => open,
												DATA_READY_PULSE => data_ready_pulse_net,
												DATA => data_net);


DelSig: DeltaSigma port map(	value => delta_sigma_in,
										clk => CLK,
										bitstream => VOUTN);


LED0 <= delta_sigma_in(0);
LED1 <= delta_sigma_in(1);
LED2 <= delta_sigma_in(2);
LED3 <= delta_sigma_in(3);
LED4 <= delta_sigma_in(4);
LED5 <= delta_sigma_in(5);
LED6 <= delta_sigma_in(6);
LED7 <= delta_sigma_in(7);

end Behavioral;

