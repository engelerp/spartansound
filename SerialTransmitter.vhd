----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:19:41 05/28/2018 
-- Design Name: 
-- Module Name:    SerialTransmitter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SerialTransmitter is
    Port ( DATA : in  STD_LOGIC_VECTOR (7 downto 0); --Packet to send. This has to be kept constant during the transmission by the sending device.
           SEND : in  STD_LOGIC; --Trigger sending on high
           CLK : in  STD_LOGIC; --200MHz clock
           BUSY : out  STD_LOGIC; --Sending is in progress
           TF : out  STD_LOGIC; --Pulses when transmission is finished
           TX : out  STD_LOGIC); --Serial output channel
end SerialTransmitter;

architecture Behavioral of SerialTransmitter is
signal transmission_line: STD_LOGIC;
signal clock_100mhz: STD_LOGIC;
signal clock_serial: STD_LOGIC;
signal transmission_finished: STD_LOGIC;
signal busy_line: STD_LOGIC;
signal idle_line: STD_LOGIC;
signal send_trigger_line: STD_LOGIC;

signal send_mask: STD_LOGIC_VECTOR (10 downto 0) := "10000000000"; --the bit that is currently sent
signal send_mask_nx: STD_LOGIC_VECTOR (10 downto 0);
signal send_data: STD_LOGIC_VECTOR (10 downto 0); --stop stop data7 data6 data5 data4 data3 data2 data1 data0 start

component ClockDivider is
    Port ( DATA_READY : in  STD_LOGIC;
           RX : in  STD_LOGIC;
           CLK_IN : in  STD_LOGIC;
           CLK_SR : out  STD_LOGIC);
end component;

begin
cldivider : ClockDivider port map (	DATA_READY => idle_line,
												RX => send_trigger_line,
												CLK_IN => clock_100mhz,
												CLK_SR => clock_serial);

--update the send_mask each time the serialclock ticks
transition: process(clock_serial)
begin
	if rising_edge(clock_serial) then
		send_mask <= send_mask_nx;
	end if;
end process;

clock_100mhz <= CLK;
TF <= transmission_finished;
BUSY <= busy_line;
idle_line <= not busy_line; --the equivalent to data ready in the receiver
send_trigger_line <= not SEND; --triggers the clock to start on low, together with high on idle

send_data(8 downto 1) <= DATA(7 downto 0); --byte that is sent
send_data(0) <= '0'; --one start bit
send_data(10 downto 9) <= "11"; --two stop bits

transmission_finished <= send_mask(9) and send_data(9); --this might cause timing issues! use the busy line to determine when we can retrigger!
busy_line <= not (send_mask(10) and send_data(10));

send_mask_nx(10 downto 1) <= send_mask(9 downto 0);
send_mask_nx(0) <= send_mask(10);
TX <= (send_mask(0) and send_data(0)) or (send_mask(1) and send_data(1)) or (send_mask(2) and send_data(2)) or (send_mask(3) and send_data(3)) or (send_mask(4) and send_data(4)) or (send_mask(5) and send_data(5)) or (send_mask(6) and send_data(6)) or (send_mask(7) and send_data(7)) or (send_mask(8) and send_data(8)) or (send_mask(9) and send_data(9)) or (send_mask(10) and send_data(10));

end Behavioral;

